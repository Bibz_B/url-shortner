import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_shortner/Models/shortmodel.dart';
import 'package:url_shortner/Support/constant.dart';
import 'package:flutter/services.dart';

class UrlCard extends StatelessWidget
{
  final Function(int) deleteItem;
  final ShortUrl url;

  UrlCard({
    @required this.url,
    this.deleteItem,
  });

  @override
  Widget build(BuildContext context)
  {
    return Container(
      height: 170,
      margin: EdgeInsets.all(16),
      child: Card(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text(url.original_link,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w300,
                          fontFamily: 'Poppins',
                          fontSize: 17
                      ),
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  color: Colors.black,
                  onPressed: (){
                    deleteItem(url.id);
                  },
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text(url.short_link,
                    style: TextStyle(
                        color: HexColor(Cyan),
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Poppins',
                        fontSize: 17
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
              ),
              color: HexColor(Cyan),
              height: 45,
              minWidth: 300,
              onPressed: (){
                Clipboard.setData(ClipboardData(text: url.short_link));
                showToast(context, 'Copied to clipboard');
              },
              child: Text(
                  'Copy',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
