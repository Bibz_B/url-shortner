import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_shortner/Models/onboardmodel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_shortner/Support/constant.dart';
import 'package:url_shortner/Ui/slider.dart';

class Onboard extends StatefulWidget {
  @override
  _OnboardState createState() => _OnboardState();
}

class _OnboardState extends State<Onboard>
{

  int currentPosition = 0;
  PageController controller = PageController();

  List<onBoard> items = [
    onBoard(image: 'assets/diagram.svg',title: 'Brand Recognition',desc: "Boost your brand recognition with each click. Generic links doesn't mean a thing. Branded links help instill confidence in your content."),
    onBoard(image: 'assets/gauge.svg',title: 'Detailed Records',desc: "Gain insights into who is clicking your links. Knowing when and where people engage with your content helps inform better decisions."),
    onBoard(image: 'assets/tools.svg',title: 'Fully Customizable',desc: "Improve brand awareness and content discoverability through customizable links, supercharging audience engagement"),
  ];

  _saveDefault() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("IntroShown", true);
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      backgroundColor: HexColor(Off_White),
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: Align(
              alignment: Alignment.topCenter,
              child: SvgPicture.asset(
                  'assets/logo.svg'
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
            child: PageView.builder(
              controller: controller,
              itemCount: items.length,
              onPageChanged: (val){
                setState(() {
                  currentPosition = val;
                });
              },
              itemBuilder: (context, index){
                return SliderView(item: items[index]);
              },
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 0, 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List<Widget>.generate(items.length, (int index) {
                      return AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        height: 10,
                        width: (index == currentPosition) ? 10 : 10,
                        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 30),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: (index == currentPosition) ? Colors.blueAccent : Colors.grey,
                        ),
                      );
                    })),
                InkWell(
                  onTap: ()  {
                    if(currentPosition == (items.length - 1))
                    {
                      _saveDefault();

                      Navigator.pushReplacementNamed(context, '/home');
                    }
                    else {
                      controller.nextPage(
                          duration: Duration(milliseconds: 800),
                          curve: Curves.easeInOutQuint);
                    }
                  },
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 300),
                    height: 50,
                    alignment: Alignment.center,
                    child: (currentPosition == (items.length - 1))
                        ? Text(
                      'Start',
                      style: TextStyle(
                          color: HexColor(Gray),
                          fontSize: 16),
                    )
                        : Text(
                      'Skip',
                      style: TextStyle(
                          color: HexColor(Grayish_Violet),
                          fontSize: 16),
                    )
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
