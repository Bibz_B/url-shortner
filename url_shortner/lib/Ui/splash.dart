import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_shortner/Support/constant.dart';

class Splash extends StatefulWidget
{
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash>
{
  bool introshown = false;

  @override
  void initState()
  {
    super.initState();
    _loadDefaults();
  }

  _loadDefaults() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    introshown = prefs.getBool('IntroShown') ?? false;
  }

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;

    double bottomspace = 0;

    if(height > 400)
    {
      bottomspace = 20;
    }
    else {
      bottomspace = 40;
    }

    Future.delayed(Duration(seconds: 1), ()
    {
      if (introshown)
      {
        Navigator.pushReplacementNamed(context, '/home');
      }

    });

    return Scaffold(
      backgroundColor: HexColor(White),
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: Align(
              alignment: Alignment.topCenter,
              child: SvgPicture.asset(
                'assets/logo.svg'
              ),
            ),
          ),
          Column(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(0, 70, 0, 0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SvgPicture.asset(
                      'assets/illustration.svg'
                  ),
                ),
              ),
              Container(
                child: Text('More than just shorter links',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Poppins',
                ),),
              ),
              Container(
                child: Text("Build your brand's recognition and get detailed insights on how your links are performing",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: HexColor(Grayish_Violet),
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                    fontFamily: 'Poppins',
                  ),),
              ),
            ],
          ),

          Align(
            alignment: Alignment.bottomCenter,
            child: (introshown == false) ?
              Container(
              margin: EdgeInsets.only(bottom: bottomspace),
              child: FlatButton(
                minWidth: 250,
                height: 45,
                color: HexColor(Cyan),
                onPressed: (){
                  Navigator.pushNamed(context, '/onboard');
                },
                child: Text(
                    'Start',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Poppins'
                    )
                ),
              ),
            ) : SizedBox(),
          ),
        ],
      ),
    );
  }
}
