import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_shortner/Models/shortmodel.dart';
import 'package:url_shortner/Support/constant.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:url_shortner/Support/database.dart';
import 'package:url_shortner/Ui/urlcard.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>
{

  VoidCallback deleteItem(int id)
  {
    var result = DBProvider.db.deleteUrl(id);
    if(result == 1)
      {
        setState(() {

        });
      }
  }

  String url = "";

  @override
  Widget build(BuildContext context)
  {
    return Material(
      child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 8,
          child: Scaffold(
            backgroundColor: HexColor(Off_White),
            body: FutureBuilder<List<ShortUrl>>(
              future: DBProvider.db.getAllUrls(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<ShortUrl>> snapshot) {
                if (snapshot.data.length != 0) {
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      ShortUrl item = snapshot.data[index];
                      return new UrlCard(url: item,
                      deleteItem: (int id){
                        deleteItem(id);
                      },);
                    },
                  );
                } else {
                  return Stack(
                    children: [
                      Stack(
                        children: <Widget>[
                          Column(
                            children: [
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
                                child: Align(
                                  alignment: Alignment.topCenter,
                                  child: SvgPicture.asset('assets/logo.svg'),
                                ),
                              ),
                              Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.fromLTRB(0, 70, 0, 0),
                                    child: Align(
                                      alignment: Alignment.topCenter,
                                      child: SvgPicture.asset(
                                          'assets/illustration.svg'),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      "Let's get started!",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.w700,
                                        fontFamily: 'Poppins',
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 250,
                                    child: Text(
                                      "Paste your first link into the field to shorten it",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: HexColor(Grayish_Violet),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w300,
                                        fontFamily: 'Poppins',
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  );
                }
              },
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            color: HexColor(Dark_Violet),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 250,
                  child: TextFormField(
                    onChanged: (val) {
                      setState(() {
                        url = val;
                      });
                    },
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 17,
                        fontWeight: FontWeight.w300,
                        color: HexColor(Gray)),
                    decoration: InputDecoration(
                        hintText: "shorten a link here...",
                        errorStyle: TextStyle(
                          color: Colors.red[400],
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        )),
                    textAlign: TextAlign.center,
                  ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white,
                ),
                ),
                FlatButton(
                  height: 45,
                  minWidth: 250,
                  color: HexColor(Cyan),
                  onPressed: () {
                    if (url == "") {
                      showToast(context,
                          "Enter a url to shorten it");
                      return;
                    } else {
                      getHttp() async {
                        try {
                          showLoaderDialog(context);

                          var response = await Dio()
                              .get(shorten_url + url);

                          var responseData =
                              response.data;

                          var short =
                          ShortResponse.fromJson(
                              responseData);

                          if(short.ok)
                          {
                              var result = short.shortUrl;

                              Navigator.pop(context);

                              await DBProvider.db
                                  .newUrl(short.shortUrl);

                              setState(() {});
                          }
                          else{

                            Navigator.pop(context);

                            showToast(context,short.error);

                          }


                        } catch (e) {
                          Navigator.pop(context);

                          print(e);
                        }
                      }

                      getHttp();
                    }
                  },
                  child: Text('SHORTEN IT!',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Poppins')),
                ),
              ],
            ),
          ),
        )
      ],
      ),
    );

  }
}

