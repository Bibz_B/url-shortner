import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_shortner/Models/onboardmodel.dart';
import 'package:url_shortner/Support/constant.dart';

class SliderView extends StatelessWidget {

  onBoard item;

  SliderView({this.item});

  @override
  Widget build(BuildContext context) {

    PageController controller;
    
    return Container(
      color: HexColor(Off_White),
      child: Stack(
        children: <Widget>[
          Center(
            child: Stack(
              children: [
                Container(
                  height: 275,
                  margin: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Text(
                          item.title,
                          style: TextStyle(
                              fontSize: 25.0,
                              fontWeight: FontWeight.w700,
                              fontFamily: "Poppins"
                          ),
                        ),
                        margin: EdgeInsets.only(top: 70),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          item.desc,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Poppins"
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 70,
                  decoration: BoxDecoration(
                    color: HexColor(Dark_Violet),
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                        item.image,
                        width: 30,
                        height: 30,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
