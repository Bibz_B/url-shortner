import 'package:flutter/material.dart';
import 'package:url_shortner/Ui/home.dart';
import 'package:url_shortner/Ui/onboard.dart';
import 'package:url_shortner/Ui/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
        '/': (context) => Splash(),
        '/onboard': (context) => Onboard(),
         '/home': (context) => Home(),
        },
    );
  }
}

