class ShortUrl
{
  int id;
  String short_link;
  String original_link;

  ShortUrl({this.short_link, this.original_link, this.id});

  factory ShortUrl.fromJson(Map<String, dynamic> json)
  {
    return ShortUrl(
      original_link: json["original_link"] as String,
      short_link: json["short_link"] as String,
    );
  }

  factory ShortUrl.fromDb(Map<String, dynamic> json)
  {
    return ShortUrl(
      original_link: json["full_url"] as String,
      short_link: json["short_url"] as String,
      id: json["id"] as int,
    );
  }

}

class ShortResponse
{
  bool ok;
  String error;
  ShortUrl shortUrl;

  ShortResponse({this.ok, this.error, this.shortUrl});

  factory ShortResponse.fromJson(Map<String, dynamic> json)
  {
    return ShortResponse(
      ok: json["ok"] as bool,
      error: json["error"] as String,
      shortUrl: ShortUrl.fromJson(json["result"]),
    );
  }
}