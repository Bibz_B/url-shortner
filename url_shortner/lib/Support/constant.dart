import 'package:flutter/material.dart';

//Colors

//Primary

 const Cyan = "#2ACFCF";
 const Dark_Violet = "#3B3054";

//Secondary

 const Red = "#F46262";

//Neutral

  const Light_Gray = "#BFBFBF";
  const Gray = "#9E9AA7";
  const Grayish_Violet = "#35323E";
  const Very_Dark_Violet = "#232127";

//Background Colors

  const White = "#FFFFFF";
  const Off_White = "#F0F1F6";

// Api

 const shorten_url = "https://api.shrtco.de/v2/shorten?url=";

void showToast(BuildContext context,String msg)
{
 final scaffold = ScaffoldMessenger.of(context);
 scaffold.showSnackBar(
  SnackBar(
   content: Text(msg),
  ),
 );
}

showLoaderDialog(BuildContext context){
 AlertDialog alert=AlertDialog(
  content: new Row(
   children: [
    CircularProgressIndicator(),
    Container(margin: EdgeInsets.only(left: 7),child:Text("Loading..." )),
   ],),
 );
 showDialog(barrierDismissible: false,
  context:context,
  builder:(BuildContext context){
   return alert;
  },
 );
}