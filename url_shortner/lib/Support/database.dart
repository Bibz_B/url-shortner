
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:url_shortner/Models/shortmodel.dart';

class DBProvider
{
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async
  {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "History.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE History ("
              "id INTEGER PRIMARY KEY,"
              "short_url TEXT,"
              "full_url TEXT"
              ")");
        });
  }

  newUrl(ShortUrl newUrl) async
  {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM History");

    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into History (id,short_url,full_url)"
            " VALUES (?,?,?)",
        [id, newUrl.short_link, newUrl.original_link]);

    return raw;
  }

  Future<List<ShortUrl>> getAllUrls() async
  {
    final db = await database;
    var res = await db.rawQuery("SELECT * FROM History");
    List<ShortUrl> list =
    res.isNotEmpty ? res.map((c) => ShortUrl.fromDb(c)).toList() : [];
    return list;
  }

  deleteUrl(int id) async
  {
    final db = await database;
    return db.rawDelete("Delete from History WHERE id = $id");
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from History");
  }

  dropTable() async {
    final db = await database;
    db.execute("DROP TABLE IF EXISTS History");
  }
}